<?php if ( !defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

/**
 * @developer Muhammad Khalil
 * @purpose To debug application errors
 */
class Log extends Public_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->helper('debug_helper');
    }


    public function index() {
        redirect('log/show');
    }

    public function show() {
//        echo anchor('log/clear', 'Clear Log', 'style="  background: #FF8585;
//        text-decoration: none;
//        font-family: arial;
//        padding: 10px;
//        margin: 6px;
//        color: #fff;
//        border-radius: 20px;
//        box-shadow: 2px 2px #999;
//        top: 6px;
//        position: relative;
//        margin-bottom: 20px;
//        display: block;
//        text-align: center;"');
//
//        echo anchor('log/show', 'Refresh Log', 'style="  background: #46A54B;
//        text-decoration: none;
//        font-family: arial;
//        padding: 10px;
//        margin: 6px;
//        color: #fff;
//        border-radius: 20px;
//        box-shadow: 2px 2px #999;
//        top: 6px;
//        position: relative;
//        margin-bottom: 20px;
//        display: block;
//        text-align: center;"');

        echo showLog();
    }

    public function clear() {
        clearLog();
        redirect('log');
    }

    public function test() {
        logIt('Will add collapse feature', __FUNCTION__);

        logIt('Log without collapse feature', __FUNCTION__, 'simple');

    }

}
